#!/usr/bin/python

import crypt

def testPass(cryptPass):
    salt = cryptPass[0:19]
    dictionaryFile = open('MyRockYou1.txt','r')
    for word in dictionaryFile.readlines():
        word = word.strip('\n')
        cryptWord = crypt.crypt(word, salt)
        if (cryptWord == cryptPass):
            print "[+] Found password: " + word + "\n" 
            return
    print "[-] Password not found. \n"
    return

def main():
    shadowFile = open("shadow",'r')
    for line in shadowFile.readlines():
        if ":" in line:
            user = line.split(':')[0]
            crypPass = line.split(':')[1].strip(' ')
            print "[*] Cracking password for user: "+ user
            testPass(crypPass)
        

main()